import axios from 'axios'; 

class Atendente{

    connection = axios.create({baseURL: 'http://localhost:3000/'});


    findAll(){
        return this.connection.get('/Atendente');
    }

    findById( id ){
        return this.connection.get('/Atendente/'+id);
    }

    save( Atendentes ){

        if(Atendentes.id == undefined){
            return this.connection.post('/Atendente/', Atendentes)
        } 
        else {
            return this.connection.put('/Atendente/'+Atendentes.id, Atendentes)
        }

    }

    delete(id){
        return this.connection.delete('/Atendente/'+id);
    }

}

export default new Atendente();