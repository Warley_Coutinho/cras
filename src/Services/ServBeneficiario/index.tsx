import axios from 'axios'; 

class ServBeneficiario{

    connection = axios.create({baseURL: 'http://localhost:3000/'});


    findAll(){
        return this.connection.get('/Beneficiario');
    }

    findById( id ){
        return this.connection.get('/Beneficiario/'+id);
    }

    save( Cadastros ){

        if(Cadastros.id == undefined){
            return this.connection.post('/Beneficiario/', Cadastros)
        } 
        else {
            return this.connection.put('/Beneficiario/'+Cadastros.id, Cadastros)
        }

    }

    delete(id){
        return this.connection.delete('/Beneficiario/'+id);
    }

}

export default new ServBeneficiario();