import React from 'react';
import { FlatList, SafeAreaView, View, StyleSheet } from "react-native";
import { Button, Icon  } from 'react-native-elements';
import ServBeneficiario from "../../../../../src/Services/ServBeneficiario";
import UserBeneficiario from "../../../../../src/Components/UserBeneficiario";

class Lista extends React.Component{

    

    constructor(props){
        super(props);
        this.state = { DATA: [] }

        this.props.navigation.setOptions ({
            
            title:  "Listas dos Beneficiarios",
            headerRight: () => (
                <View style={styles.container} >


                    <Button
                        type="clear"
                        icon={ <Icon name='add-circle' type='ionicon' size={30} color='red' /> }
                        onPress={ () => this.onPressNew() }
                    />

                </View>
            ),

        });

    }

    onPressNew = () => {
        this.props.navigation.navigate('Formulario',{beneficiario: {} });
    }

    componentDidMount(){
        ServBeneficiario.findAll().then( response => this.setState( {DATA: response.data }) )
    }

    render(){
        return(
            <SafeAreaView >
                <FlatList
                    data={this.state.DATA}
                    renderItem={UserBeneficiario}
                    keyExtractor={(item) => item.id}
                />
            </SafeAreaView>
        )
    }

}

export default Lista;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1ecec',
        flexDirection: 'row',
        alignItems: "center",
        padding: 10
    }
});