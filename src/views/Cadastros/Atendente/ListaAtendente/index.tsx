import React from 'react';
import { FlatList, SafeAreaView, View, StyleSheet } from "react-native";
import { Button, Icon  } from 'react-native-elements';
import ServAtendente from "../../../../../src/Services/ServAtendente";
import UserAtendente from "../../../../../src/Components/UserAtendente";

class ListaAtendente extends React.Component{

    

    constructor(props){
        super(props);
        this.state = { DATA: [] }

        this.props.navigation.setOptions ({
            
            title:  "Listas dos Atendentes",
            headerRight: () => (
                <View style={styles.container} >


                    <Button
                        type="clear"
                        icon={ <Icon name='add-circle' type='ionicon' size={30} color='red' /> }
                        onPress={ () => this.onPressNew() }
                    />

                </View>
            ),

        });

    }

    onPressNew = () => {
        this.props.navigation.navigate('FormularioAtendente',{atendente: {} });
    }

    componentDidMount(){
        ServAtendente.findAll().then( response => this.setState( {DATA: response.data }) )
    }

    render(){
        return(
            <SafeAreaView >
                <FlatList
                    data={this.state.DATA}
                    renderItem={UserAtendente}
                    keyExtractor={(item) => item.id}
                />
            </SafeAreaView>
        )
    }

}

export default ListaAtendente;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f1ecec',
        flexDirection: 'row',
        alignItems: "center",
        padding: 10
    }
});