
# Install JSON Server
npm install -g json-server

# Executar o Banco de dados Json Server
json-server --watch db.json

# Executar as dependencias do Projeto
npm install

# Executar o projeto
npm start


