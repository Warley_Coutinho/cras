import React from 'react';
import { StyleSheet} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import Login from './src/views/Login'
import Atendente from './src/Dashboard/Atendente';
import Supervisor from './src/Dashboard/Supervisor';
import Lista from './src/views/Cadastros/Beneficiario/Lista';
import Detalhes from './src/views/Cadastros/Beneficiario/Detalhes';
import Formulario from './src/views/Cadastros/Beneficiario/Formulario';
import ListaAtendente from './src/views/Cadastros/Atendente/ListaAtendente';
import DetalhesAtendente from './src/views/Cadastros/Atendente/DetalhesAtendente';
import FormularioAtendente from './src/views/Cadastros/Atendente/FormularioAtendente';


const Stack = createStackNavigator();


export default function App() {
  return (

      <NavigationContainer>
        <Stack.Navigator initialRouteName="Login">

       <Stack.Screen name="Login" component={Login}/> 
        
          
          <Stack.Screen name="Atendente" component={Atendente} />
          <Stack.Screen name="Supervisor" component={Supervisor}  />
          <Stack.Screen name="Lista" component={Lista} />
          <Stack.Screen name="Detalhes" component={Detalhes} />
          <Stack.Screen name="Formulario" component={Formulario} />
          <Stack.Screen name="ListaAtendente" component={ListaAtendente} />
          <Stack.Screen name="DetalhesAtendente" component={DetalhesAtendente} />
          <Stack.Screen name="FormularioAtendente" component={FormularioAtendente} />
         

        </Stack.Navigator>
      </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e9e5e5',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
